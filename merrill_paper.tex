%\documentclass[gc]{AGUTeX}

\documentclass[draft,gc]{AGUTeX}
\usepackage{lineno}
\linenumbers


% Author names in capital letters:
\authorrunninghead{\'O~CONBHU\'I ET AL.}

% Shorter version of title entered in capital letters:
\titlerunninghead{MERRILL}

% Corresponding author mailing address and e-mail address:
\authoraddr{
Corresponding author: P\'adraig \'O~Conbhu\'{\i}, Department of
Geosciences, University of Edinburgh, Edinburgh, United Kingdom.
(poconbhui@ed.ac.uk)
}

%\usepackage{parskip}



\begin{document}


%
% Title
%
\title{
    MERRILL:
    Micromagnetic Earth Related Robust Interpreted Language Laboratory
}


%
% Authors
%
\authors{
    P\'adraig \'O~Conbhu\'{\i} \altaffilmark{1},
    Wyn Williams \altaffilmark{1},
    Karl Fabian \altaffilmark{2},
    Phil Ridley \altaffilmark{3},
    and Lesleis Nagy \altaffilmark{1}
}

\altaffiltext{1}{
Department of Geosciences, University of Edinburgh, Edinburgh, United Kingdom.
}

\altaffiltext{2}{
Geological Survey of Norway,
Leiv Eirikssons vei 39,
7491 Trondheim,
Norway
}

\altaffiltext{3}{Cray UK Ltd, Broad Quay House, Broad Quay, Bristol BS1 4DJ}


%
% Abstract
%
\begin{abstract}
    MERRILL is an easy to use 3D micromagnetics package with a simple scripted
    interface. It uses the Finite Element Method, so it can be used with
    cubic and non-cubic geometries like octahedra and spheres, and
    irregular geometries such as realistic grains that might be extracted
    from tomographic images.
    It is suitable for simulations of grains up to hundreds of
    nanometers in size, and for simulations of several interacting grains.
    It is particularly well suited for performing simulated hysteresis loops,
    and includes new techniques for finding the energy barriers between
    distinct remanent states, which can be used to determine the thermal
    stability of the state.
\end{abstract}


\begin{article}

%
% Introduction
%
\section{Introduction}

3-Dimensional micromagnetic models were first introduced in the late 1980’s
\citep{Schabes1988,Williams1989}, and over the last 30 years have contributed to a wealth of information on
magnetic properties of interacting and non-uniformly magnetized particles.
In paleomagnetism, for example,  such studies have demonstrated that small PSD
grains, ubiquitous in rocks, primarily occupy single-vortex (SV) states and that
these states can provide reliable and stable recording of the ancient magnetic
field.
Modern desktop computers and workstations are now powerful enough to perform
micromagnetic simulations of a wide range of grains sizes, materials and even
clusters of grains that are of interest to rock, paleo and environmental
magnetists.
We describe here a finite element micromagnetic modelling package designed for
the earth science community.
This is a script based modelling program that needs no specialist computing
knowledge, no proprietary additional software to run the models and visualize
the magnetic domain structures.
The software and source code is freely available and runs on LINUX, OSX and
Windows.


%
% Micromagnetism
%
\section{Micromagnetism}

In micromagnetism, we describe a magnetic system in terms of a continuous
vector valued function $\vec{M}(\vec{x})$ called the magnetization
\citep{brown1963micromagnetics}.
The magnetization at a given point is constructed by averaging the small and
point-like sources of magnetism, e.g. electron spins and orbitals, over a small
volume centered at that point.
The volume used is large enough that the behaviour of individual atoms are
averaged out, but small enough to resolve inhomogeneous magnetic structures
such as domain walls, vortices, flower states etc.
This averaging and interpolation technique is called the continuum
approximation, and the value of the magnetization at a given point is expected
to be the average over a number of, say 10 or 100, atoms.

In this approximation, given a micromagnetic energy $E(\vec{M})$,
the effective field $\vec{H}$ is given as a function of $\vec{M}$
\begin{equation}
    H^\texttt{eff}_i(\vec{M}) = -\frac{\partial E}{\partial M_i}(\vec{M})
\end{equation}
The effective field can be thought of as a ``force'' term on the magnetization.
The dynamics of the system can be described by the LLG equation
\begin{equation}
    \frac{\partial M}{\partial{t}} =
        - \gamma \vec{M} \times \vec{H}^\texttt{eff}(\vec{M})
        - \lambda \vec{M} \times (\vec{M} \times \vec{H}^\texttt{eff}(\vec{M}))
\end{equation}
where $\gamma$ is the electron gyromagnetic ratio and $\lambda$ is a material
dependant damping factor.
This behaves like a dynamical system where a force $\vec{H}^\texttt{eff}$ acts
on a system with a non-zero angular momentum vector pointing parallel to
$\vec{M}$.
A sufficient condition for an equilibrium magnetization $\vec{M}^0$ is given by
\begin{equation}
    \label{eqn:llg_equilibrium_solution}
    \vec{M}^0 \times \vec{H}^\texttt{eff}(\vec{M}^0) = 0
\end{equation}
In rock magnetism, this solution is called a remanent magnetization state,
and we will denote it $\vec{M}^\texttt{rem}$.
From equation (\ref{eqn:llg_equilibrium_solution}) we can see two types of
solution.
Either
$\vec{M}^0$ is parallel to $\vec{H}^\texttt{eff}(\vec{M}^0)$
or
$\vec{H}^\texttt{eff}(\vec{M}^0) = 0$. In MERRILL, we focus on the second
solution, as this results in stable solutions, i.e. the sort one would
expect to find in nature.

A remanence state $\vec{M}^\texttt{rem}$ satisfies the following constraints
\begin{eqnarray}
    \hspace{0.3in}
    \label{eqn:min_energy_grad_soln}
    - H^\texttt{eff}_i(\vec{M}^\texttt{rem})
        = \frac{\partial E}{\partial M_i}(\vec{M}^\texttt{rem})
        &=& 0_i
    \\
    \label{eqn:stable_energy_equilibrium}
    \frac{\partial^2 E}{\partial M_i^2}(\vec{M}^\texttt{rem})
        &>& 0
    \\
    \label{eqn:m_constant_length}
    \vec{M}^\texttt{rem}
        &=& M_s \frac{\vec{M}^\texttt{rem}}{|\vec{M}^\texttt{rem}|}
\end{eqnarray}
Constraints (\ref{eqn:min_energy_grad_soln}) and
(\ref{eqn:stable_energy_equilibrium}) suggest that a remanence state represents
a local minimum solution of the micromagnetic energy. By a local energy
minimum, we mean for some $\vec{M}^\texttt{rem}$,
any small change away from that state will increase the energy.
This can be written, for any small value $\vec{\epsilon}$ as
\begin{equation}
    \label{eqn:rough_lem_explanation}
    E(\vec{M}^\texttt{rem}) \le E(\vec{M}^\texttt{rem} + \vec{\epsilon})
\end{equation}
It is important to note
that the constraint in equation (\ref{eqn:m_constant_length}) states that the
magnetization is a vector of constant length.  This is true at all times, even
outside of equilibrium. This fact means finding a solution for
$\vec{M}^\texttt{rem}$ involves the use of non-linear solving techniques, since
each component of $\vec{M}$ and its derivatives have a square root dependency
on the other components.


%
% Effective Fields
%
\subsection{Effective Fields}

The effective field $\vec{H}^\texttt{eff}(\vec{M})$ for a typical cubic
ferromagnetic crystal involves four primary components: Zeeman, anisotropy,
exchange and demagnetizing fields \citep{brown1963micromagnetics,Kittel1949}.


%
% Zeeman Field
%
\subsubsection{Zeeman Field}
The Zeeman field represents the interaction of external sources of magnetic
field with the magnetic system under investigation.
As such, it is often referred to as the ``external field.''
It is assumed that the system under investigation has no effect upon the
external source.
\begin{equation}
    \vec{H}^\texttt{zemn} = \texttt{constant}
\end{equation}


%
% Anisotropy Field
%
\subsubsection{Anisotropy Field}
The anisotropy field couples the magnetization to the crystal lattice.
It is the primary mechanism by which the symmetries of the lattice affect the
magnetization. For a cubic, ferromagnetic crystal, if the crystal axes are
along the $x$, $y$, and $z$ coordinate axes, it can be written
\begin{equation}
    \vec{H}^\texttt{anis} = 
        2 K_1 \left(
            \alpha_1 ( \alpha_2^2 + \alpha_3^2 )
        \ ,\ 
            \alpha_2 ( \alpha_3^2 + \alpha_1^2 )
        \ ,\ 
            \alpha_3 ( \alpha_1^2 + \alpha_2^2 )
        \right)
\end{equation}
with $K_1$ the anisotropy constant.

The vector $\vec{\alpha}$ here represents the directional cosines of the
magnetization with respect to the crystal axes.
For a crystal with cubic axes $\vec{a}$, $\vec{b}$, and $\vec{c}$, the
vector $\vec{\alpha}$ is defined
\begin{equation}
    \vec{\alpha} = \left(
        \frac{\vec{M} \cdot \vec{a}}{|\vec{M}| |\vec{a}|}
        \ ,\ 
        \frac{\vec{M} \cdot \vec{b}}{|\vec{M}| |\vec{b}|}
        \ ,\ 
        \frac{\vec{M} \cdot \vec{c}}{|\vec{M}| |\vec{c}|}
    \right)
\end{equation}
such that $\vec{\alpha} \cdot \vec{\alpha} = 1$.

For a non-cubic material, often only the anisotropy energy needs to
be changed. For a crystal with a uniaxial symmetry, e.g. a hexagonal crystal,
a uniaxial anisotropy
$\vec{H}^\texttt{anis}(\vec{M}) = K_1 (\vec{M} \cdot \vec{a})^2$
is commonly used, with $a$ the anisotropy axis. For ferromagnetic-like
materials, e.g. ferrimagnetic, no other change is necessary.


%
% Exchange Field
%
\subsubsection{Exchange Field}
The exchange field serves to align nearest neighbour magnetizations.
This incorporates quantum mechanical spin coupling. A micromagnetic expression
can be given:
\begin{equation}
    \vec{H}^\texttt{exch} = A \nabla^2 \vec{\alpha}
\end{equation}
with $A$ the exchange coupling constant.


%
% Demagnetizing Field
%
\subsubsection{Demagnetizing Field}
The demagnetizing field represents the magnetic field generated by the
magnetic material itself, derived from the ``magnetic self-energy.''
It is called the demagnetizing field, because a
higher demagnetizing field represents a higher energy configuration, so it
typically acts on the magnetization in such a way that it minimizes itself.
%
\begin{eqnarray}
    \hspace{1in}
    \label{eqn:demag_field_start}
    \vec{H}^\texttt{dmag}
        &=& \vec{\nabla} \phi
    \\
    \nabla^2 \phi
        &=& \vec{\nabla} \cdot \vec{M}
    \\
    \label{eqn:demag_field_end}
    \phi(\infty)
        &=& 0
\end{eqnarray}
%
Outside of the magnetic material, this is the ``stray field.''


When solving for the minimum energy of equation
(\ref{eqn:min_energy_grad_soln}), we use
\begin{eqnarray}
    \hspace{0.3in}
    \label{eqn:micromagnetic_solution_from_heff}
    \vec{H}^\texttt{eff}(\vec{M})
    &=&
    \vec{H}^\texttt{exch}(\vec{M})
    +
    \vec{H}^\texttt{anis}(\vec{M})
    \\
    \nonumber
    &&
    +
    \vec{H}^\texttt{zemn}(\vec{M})
    +
    \vec{H}^\texttt{dmag}(\vec{M})
\end{eqnarray}
To include other phenomena in the theory, one need only derive the
effective field, typically by taking the derivative of the energy with
respect to the magnetization, and add it to
equation (\ref{eqn:micromagnetic_solution_from_heff}).


%
% Simulation
%
\section{Simulation}

To solve for the remanent magnetization state described by equations
(\ref{eqn:min_energy_grad_soln}) - (\ref{eqn:m_constant_length})
on a computer using the micromagnetic fields
(\ref{eqn:micromagnetic_solution_from_heff}), we must first be able to describe
and calculate each contribution to $\vec{H}^\texttt{eff}(\vec{M})$ in a manner
that makes sense to a computer, and then use them to find local energy minima.


%
% Calculating Fields
%
\subsection{Calculating Fields}

The Finite Element Method (FEM) is a technique for describing functions over a
geometry and solving differential equations which use those functions
\citep{Davies2011}.
It does this by turning the differential equations and the functions into
a linear algebra problem, where the solution to a matrix equation can be
converted into a solution to the differential equation.
Solving a differential equation over a given geometry
is then equivalent to solving a linear algebra problem, which is a well studied
computational problem.

In the case of micromagnetism, the functions are the magnetization $\vec{M}$ and
the effective fields $\vec{H}$, the geometry is a ferromagnetic crystal
described by a 3D tetrahedral mesh, and the
differential equations are the effective fields previously described.

The only fields which actually involve derivatives or solving differential
equations are the exchange field and the demagnetizing field.
The calculation of the exchange is relatively straightforward, being a simple
derivative of the magnetization.
However the demagnetizing field described by equations
(\ref{eqn:demag_field_start}) - (\ref{eqn:demag_field_end}), involves solving
the Poisson equation over an infinite space, since the boundary conditions are
only known at infinity.
MERRILL makes use of the Boundary Element Method (BEM)
\citep{Davies2011,Lindholm1984}, which is a
specialization of the FEM, particularly suited to problems defined over an
infinite space.
It transforms the integration to infinity to an integration over the surface of
the geometry.
The trade off here is that the runtime and the memory usage are finite, but now
dependant on the square of the number of nodes on the boundary of the material.
In our experience, this is acceptable for, say, magnetite and iron grains up to
several hundred nanometers across.


%
% Mesh Generation
%
\subsection{Mesh Generation}
With MERRILL, a magnetic material is described using a tetrahedral mesh.
A tetrahedral mesh is a tessellation of tetrahedra, which each share their
vertices, faces and edges with the other tetrahedra in the tessellation.
When solving the differential equations using the finite element method,
calculations are performed over each tetrahedron, and solutions are
found at each vertex.
That is to say, numerical stability relies on the shape of the tetrahedra, and
the accuracy of a solution relies on the spacing of the vertices in the mesh.
To this end, it is important to have a ``good'' mesh of the magnetic region
to ensure MERRILL is capable of finding solutions for the exchange fields
and demagnetizing fields, and a ``fine'' enough mesh to ensure the solutions
it finds are accurate enough for the application.

A ``good'' mesh means the tetrahedra have as large an internal angle as
possible, looking as close to a ``regular'' tetrahedron as possible.
Numerical instabilities become an issue when ``slivers'' are introduced.
These are tetrahedra where all four vertices lie in the same plane, or are
close to being planar.
The software and method used for mesh generation can be a very important
part of the process.
MERRILL comes with a basic built in cube and sphere mesh generator, however,
for more complex geometries, and external program will be needed.
A free and open source command line program, MEshRILL, is available for
generating some basic shapes such as cubes, spheres, and octahedra,
and provides a library interface for building simple C\verb!++! programs
to generate more involved geometries, such as clusters of spheres,
or arbitrary geometries from surface geometry reconstructions.
There exist many other free and commercial meshing solutions as this is a
frequently encountered problem in academia and industry.

For micromagnetic applications, a ``fine'' enough mesh is usually described in
terms of the ``exchange length'', $l_\texttt{exch}$ \citep{Rave1998a},
and the edges of the tetrahedra should not exceed this length.
The exchange length is given in terms of the magnetic material parameters
\begin{equation}
    l_\texttt{exch} = \frac{2 A}{\mu_0 M_s^2}
\end{equation}
It has its origins in the derivation of the exchange energy in terms of a
derivative of the magnetization, suitable for micromagnetic applications, and
the finite difference method where the accuracy of derivatives is dependant on
the node spacing.
With the finite element method, these restrictions can be relaxed a bit, since
derivatives are derived independent of the distance between nodes, but it is
still recommended that the distance between neighbouring nodes of a mesh stay
within the exchange length.
In particular, in the FEM the magnetization function is defined over the
tetrahedra in terms of a linear interpolation.
This is not well suited to interpolating large changes in the magnetization
between neighbouring nodes while keeping the magnetization at a fixed
length.
The exchange and demagnetizing energies will, therefore, not be accurate
if the magnetization changes by a large amount within the space of a single
tetrahedron.
The exchange length is a measure of distance where the change in magnetization
will remain ``small''.

For iron, for example, the exchange length is around 3.3~nm. A sphere of radius
100~nm, generated with MEshRRILL, has around 90,000 nodes and 500,000 elements,
with 9,000 nodes on the boundary.
These numbers will vary depending on the meshing software used and the meshing
algorithm.
For the FEM, the time taken to build the initial matrices is proportional
to the number of elements, $\mathcal{O}(N(\texttt{tetrahedra}))$.
The time taken to solve equations, i.e. find the exchange and demagnetizing
fields for a given magnetization, is roughly proportional to the number of
nodes, $\mathcal{O}(N(\texttt{nodes}))$, in the best case scenario, but will
typically be geometrically larger, depending on the connectivity between
nodes.
For the sphere generated here, that factor was around 15,
so $\mathcal{O}(15\, N(\texttt{nodes}))$.
With the addition of the BEM, an additional time proportional to the number
of boundary faces squared is needed to build the matrices,
$\mathcal{O}(N(\texttt{boundary faces})^2)$,
and an additional time proportional to the number of boundary nodes
squared to solve for the fields, $\mathcal{O}(N(\texttt{boundary nodes})^2)$.



%
% Finding Local Energy Minima
%
\subsection{Finding Local Energy Minima}
Finding a solution which satisfies the remanence magnetization state equations
(\ref{eqn:min_energy_grad_soln}) - (\ref{eqn:m_constant_length})
is equivalent to finding a local energy minimum (LEM) as described by
equation (\ref{eqn:rough_lem_explanation}).
For a physically stable solution, it is necessary only to find
a solution $\vec{M}^\texttt{rem}$ which is in a sufficiently deep
``energy well'' that a small perturbation due to e.g. thermal fluctuation is
unlikely to allow it to escape.
We want, for a reasonably small $\vec{\epsilon}$
\begin{equation}
    E(\vec{M}^\texttt{rem}) \ll E(\vec{M}^\texttt{rem} + \vec{\epsilon})
\end{equation}
However, the definitions of a ``reasonably small'' $\vec{\epsilon}$ and
a ``large'' difference in energies for a single minimization
are open to interpretation.

MERRILL makes use of a ``Hubert Minimizer'' scheme
\citep{Ramstoeck1997,Berkov1998}, which can be described roughly as an
accelerated steepest descent algorithm across the energy landscape.
The energy landscape is a term used to think about
the energy as a ``height'' at a position $\vec{M}$. As $\vec{M}$ changes,
so too does your position on the energy landscape, producing a new
energy, and so a new height. One can think of varying $\vec{M}$ as
travelling in some direction, and the relative change in energy as going
uphill or downhill, or travelling along flat ground, possibly on a plane
or in a valley.

The Hubert Minimizer is an iterative scheme which moves $\vec{M}$ as
$\vec{M}_{i+1} = \vec{M}_i + \alpha \vec{H}^\texttt{eff}(\vec{M}_i)$ where
$\alpha$ is a ``step size'', with $\vec{M}_{i+1}$ then renormalized to unit
length, conforming with equation (\ref{eqn:m_constant_length}).
When the minimizer encounters a net negative
slope in the energy landscape, it will increase its step size. When it
encounters a net positive slope, it will decrease its step size. When it is on a
plane, it will continue along the previous direction and increase its step size.
As the minimizer approaches an energy minimum, $\vec{H}^\texttt{eff}$ will start
approaching zero, so the minimizer should be self-terminating, similar
to the self-termination of a Conjugate Gradient minimizer.

Due to the non-linear nature of the problem, owing to the constant length
of $\vec{M}$, this can be more stable than, e.g. a Conjugate Gradient
minimization which assumes a reasonably concave energy landscape.
It has the added benefit that the acceleration term means
solutions are less likely to become trapped in a shallow LEM, as it may
have enough ``momentum'' to escape the well.


%
% BEMScript: The MERRILL Scripting Language
%
\section{BEMScript: The MERRILL Scripting Language}

We present a simple scripting language for use with MERRILL, BEMScript.
A BEMScript is a file with a series of commands for MERRILL to execute.
These range from setting material constants, to loading meshes, to finding
a local energy minimum.

A complete listing of the commands can be found in the MERRILL documentation,
which can be found alongside the source code.
Some typical commands include (but are not limited to)
\begin{description}
    \item{\bf Magnetite {\it Temperature} C}\\
        Set material constants to magnetite at {\it Temperature} degrees
        Celsius.
    \item{\bf Iron {\it Temperature} C}\\
        Set material constants to iron at {\it Temperature} degrees
        Celsius.
    \item{\bf GenerateCubeMesh {\it Width} {\it EdgeLen}}\\
        Generate a cubic geometry of width {\it Width} and with the average
        length between mesh nodes of length {\it EdgeLen}.
    \item{\bf Uniform Magnetization {\it X} {\it Y} {\it Z}}\\
        Set the magnetization of the material to
        \[ \vec{M} = M_s \cdot \texttt{norm}((X, Y, Z)) \]
    \item{\bf Minimize}\\
        Run the minimizer to find the local energy minimum.
    \item{\bf WriteMagnetization {\it SolutionBase}}\\
        Write the magnetization to disk. Two files are written,
        {\it SolutionBase}.dat and {\it SolutionBase}\_mult.tec.
        The file {\it SolutionBase}.dat contains a list of vertex points and the
        magnetization at that point.
        This is suitable for use with the {\bf ReadMagnetization} command, which
        will be the next command described.
        The {\it SolutionBase}\_mult.tec file is the mesh and the magnetization
        in a TecPlot file format.
        This can be read by a number of visualization tools, including the free,
        open source and cross-platform ParaView software \citep{Ahrens2005}.
    \item{\bf ReadMagnetization {\it MagnetizationFile}}\\
        Read a previously written {\it MagnetizationFile} from the
        {\bf WriteMagnetization} command, and set the current magnetization
        based on that.
    \item{\bf External Field Direction {\it X} {\it Y} {\it Z}}\\
        Set the Zeeman field direction to
        \[ \hat{H}^\texttt{zemn} = \texttt{norm}( (X, Y, Z) ) \]
    \item{\bf External Field Strength {\it Magnitude} mT}\\
        Set the Zeeman field strength to
        \[ |H^\texttt{zemn}| = Magnitude \]
\end{description}

BEMScript also includes supports for variables and loops. For example,
the script
\begin{verbatim}
Loop myvalue 0 100 10
    print %myvalue
EndLoop
\end{verbatim}
will print out the values 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100.
To distinguish between floating point, integer and string values, the
variable {\it myvalue} can be referred to as
\verb!%myvalue!, \verb!#myvalue! and \verb!$myvalue$!, depending on
what is needed.
For example, in
\begin{verbatim}
Loop myvalue 0 100 10
    WriteMagnetization SolnFileA_%myvalue
    WriteMagnetization SolnFileB_#myvalue
EndLoop
\end{verbatim}
the filenames written to by the two {\bf WriteMagnetization} commands are
quite different. The argument \verb!SolnFileA_%myvalue! produces filenames
of the form SolnFileA\_+0.10000E2 when \verb!myvalue! is 10. The
argument \verb!SolnFileB_#myvalue! produces filename of the form
SolnFileB\_10 when \verb!myvalue! is 10. This can be useful to avoid headaches
in file enumeration, if nothing else.


We will now outline a few computer experiments that can be used to probe the
behaviour of the magnetization of magnetic materials in MERRILL.
This will also serve as a brief tutorial on the BEMScript language and some
features in MERRILL.


%
% Minimization
%
\subsection{Minimization}

A basic minimization from a random start, or a uniformly magnetized start
can be a quick way to tell if a grain is around the SD or PSD range.
Moreover, it is the basic building block from which all other measurement
techniques come.

It is important to note that there may be several magnetizations the
minimization might settle upon, representing the several local energy minima,
and so the several potential remanent magnetization states of the grain.
However, from a given starting point, barring numerical noise,
the minimization should always reach the same end point.

A simple minimization script for a Magnetite cube can be given

\begin{verbatim}
! Setup material constants for Magnetite at 20
! Degrees Celsius
Magnetite 20 C

! Generate a cube using the built-in cube mesh
! generator
GenerateCubeMesh 0.18 0.005

! Run the minimizer
Minimize

! Output the solution M to two files: soln.dat and
! soln_mult.tec.  The soln.dat file can be used as
! the input for another run as, say, an initial
! guess.  soln_mult.tec is a TecPlot format file
! which can be used to view the solution in a
! visualization program such as ParaView (which is
! free and open source!)
WriteMagnetization soln
\end{verbatim}


%
% Hysteresis Loops
%
\subsection{Hysteresis Loops}

Hysteresis loops are a useful tool when a magnetic material has several
remanent magnetization states for a given set of parameters.
From a hysteresis loop, it is possible to deterministically move from
one state to another, and find the tipping point where the variation
of a given parameter will cause one state spontaneously switch to the other.
This also provides information on the range of values for the given parameter
where both states can co-exist.
In other words, it provides some information
about the stability of a given state when multiple valid states exist.

Since the minimization should always reach the same solution when given the
same starting point, a hysteresis loop run with the same parameters and the
same changes in parameters should always return the same results in MERRILL.


%
% Magnetic Field Hysteresis
%
\subsubsection{Magnetic Field Hysteresis}

A magnetic hysteresis loop has several uses.
A single hysteresis loop can be useful
to get a feel for the behaviour of the magnetization of a system (i.e. its
coercivity, if it's SD or PSD). The average of hysteresis loops in many
directions can be used to compare simulations with experimental observations
of magnetic characteristics such has coercivity.

A hysteresis loop is a quasi-static thermodynamic process. That is to say, at
each point of the hysteresis loop, the system is assumed to be in equilibrium
and the energy at a local energy minimum.
A change from one point to the next in a hysteresis loop represents the system
moving from what was a local energy minimum in the previous step to the nearest,
new local energy minimum in the current step as an external field is varied.

The simulation of a hysteresis loop is accomplished by the following scheme
\begin{enumerate}
    \item
        Set Zeeman field to saturating value in a fixed direction: Z = Zmax
    \item
        Find LEM
    \item
        Update Zeeman field: Z = Z - a*Zmax
    \item
        If Z != -Zmax, Go to 2
\end{enumerate}
For a saturating field $\vec{Z}^\texttt{max}$, a hysteresis loop will run from
$\vec{Z}^\texttt{max}$ to -$\vec{Z}^\texttt{max}$ in small increments. Here,
``small'' means sufficiently small enough that any phenomenon sensitive to the
external field, e.g. magnetization switching, nucleation of vortices,
are accounted for.
In practice, ``small'' means just small enough to resolve changes in the
magnetization you are looking for, but as large as possible to reduce the
total number of steps needed for the loop. Otherwise a large amount of work
may be done unnecessarily for little progress.

A MERRILL script which accomplishes this is
\begin{verbatim}
Magnetite 20 C
GenerateCubeMesh 0.18 0.005

External Field Direction 1 0 0
External Field Strength 0 mT

Loop field -100 100 5
    Randomize Magnetization 10 mT
    External Field Strength %field
    Minimize
    ! Write current value to disk, so we can
    ! inspect it later.
    WriteHyst hyst_soln
EndLoop
\end{verbatim}
Due to symmetry, it is only necessary to run either the upper of lower branch
of the hysteresis loop, not both.


%
% Size Hysteresis
%
\subsubsection{Size Hysteresis}

A hysteresis loop where the size of the grain is varied rather than an external
field can be a useful tool for determining SD and PSD ranges, and the
evolution of magnetic domain states.
Since several remanent states can exist for a grain,
particularly in the early PSD size range,
it can be difficult to pinpoint exactly where that regime begins.

In a size hysteresis loop, the grain is started in an SD magnetization state.
The size of the grain is increased until it spontaneously switches to a vortex
PSD state. The size of the grain is then scaled down until it's in an SD
state again. The branch of increasing size can tell us what the largest
grain size is which supports an SD state, and the branch of decreasing size
can tell us what the smallest grain size is which supports a PSD state.

Note that nothing has been said about what exactly the initial SD state should
be. Ideally, a size hysteresis should be run for each possible SD remanent
state. For symmetric grains, symmetry should make many of these redundant.
However, for asymmetric grains, the direction of the initial magnetization
may greatly affect the stability of the SD solution.

When scaling sizes, it is important to consider the size of the elements of the
mesh during scaling. Typically, the edges of a mesh should be around the
exchange length.
When the size of a mesh is increased, the average edge length should not exceed
the exchange length.
By starting with a larger mesh and scaling down, rather than the other way
around, we can avoid this problem. However, finer meshes take longer to run.
A compromise can be found using the ``RemeshTo'' command. A user might use
a mesh that is suitable for scaling up to, say, 0.1~$\mu$m, and at that
size, switch to a mesh suitable for up to, say, 0.2~$\mu$m.

In MERRILL, the ``Resize'' command can be used to scale the mesh.
An example size hysteresis script incorporating all of this:
\begin{verbatim}
! Use magnetite material parameters
Magnetite 20 C

! Ensure we can load at least 2 meshes at a time
Set MaxMeshNumber 2

! Load 100nm mesh into slot 1
ReadMesh 1 octahedron_0.1um.neu
! Load 200nm mesh into slot 2
ReadMesh 2 octahedron_0.2um.neu

! Make sure mesh 1 is loaded
LoadMesh 1

! Loop from 10 to 100 in steps of 10
Loop meshsize 10 100 10
    ! Resize our mesh to the current %meshsize
    ! For %meshsize 20, for example, the 0.1um
    ! mesh is scaled to 0.02um.
    Reize 100 %meshsize

    ! Run the minimization
    Minimize

    ! Write the output to a file
    WriteMagnetization size_hysteresis_%meshsize

    ! Resize the mesh back to its original size,
    ! for the next loop iteration
    Resize %meshsize 100
EndLoop

! Hand off to the 0.2um mesh

! Interpolate the current magnetization to mesh 2
Remesh 2

! Load mesh 2
LoadMesh 2


! Loop from 110 to 200 in steps of 10
Loop 110 200 10
    ! Resize our mesh to the current meshsize
    Reize 200 %meshsize

    ! Run the minimization
    Minimize

    ! Write the output to a file
    WriteMagnetization size_hysteresis_%meshsize

    ! Resize the mesh to the reference size, for
    ! the next loop iteration
    Resize %meshsize 200
EndLoop
\end{verbatim}


%
% Temperature Hysteresis
%
\subsubsection{Temperature Hysteresis}

A temperature hysteresis can be used to observe the effects of heating, say
iron, from room temperature to the Curie temperature and back.
The approach MERRILL takes to this is to simply change the temperature
dependant material parameters for each step.
In this way, the model is of a ``cold'' material, since no other thermal
effects are taken into account which might spontaneously and
non-deterministically effect the magnetization.
However, the changes in material properties can have significant impact
on the shape of the energy landscape with respect to the depths, positions
and even number of the local energy minima.

An example temperature hysteresis of iron can be given

\begin{verbatim}
GenerateCubeMesh 0.18 0.005

Uniform Magnetization 1 1 1

Loop temperature 20 770 10
    ! Set the material parameters for iron at the
    ! given temperature
    Iron %temperature C

    ! Find the LEM
    Minimize

    ! Write current value to disk, so we can
    ! inspect it later.
    WriteHyst temperature_hyst_soln_#temperature
EndLoop
\end{verbatim}


%
% Energy Barriers
%
\subsection{Energy Barriers}
MERRILL includes a new method for finding the minimum energy transition between
to given remanent states. It uses the Nudged Elastic Band method and an action
minimization technique to do this \citep{Berkov1998,Fabian2017}.
This constructs a range of intermediary states between the two remanent states
which should represent the physical path taken when a grain switches from one
state to the other.
By finding the path with the lowest maximum energy barrier, it is possible to
say what energy the grain must have to switch between these states, and from
that, assign a probability of switching due to thermal energy.

This can be used to determine the length of time a grain is expected to remain
in a given state.
If the time is on the order of seconds, one can conclude that the state is
unstable, and if it's millions of years, it is regarded as stable.
This can be useful when taking paleomagnetic measurements of a sample, for
example.
If the morphology of the paleomagnetic minerals are known (composition, geometry
etc) the NEB can provide some insight to whether the signal recorded has
remained stable since it was recorded.

An example script for an NEB calculation:

\begin{verbatim}
! Use only one mesh for this NEB minimization
set MaxMeshNumber 1

! Read in the mesh
ReadMesh 1 model.pat

! Set the maximum number of energy evaluations for
! LEM/path calculations
set MaxEnergyEvaluations 10000
set MaxPathEvaluations 1000

! Set the material parameters using a predefined
! material (iron)
iron 20 C

! Define an initial path containing only two
! points (the start and end points)
Set PathN 2

! Read in the start structure from a magnetization
! file & store as path point 1
ReadMagnetization start_mag
MagnetizationToPath 1

! Read in the end structure from a magnetization
! file & store as path point 2
ReadMagnetization end_mag
MagnetizationToPath 2

! Define the energy log output file
EnergyLog nebinitial_energy

! Refine the path to 100 structures (98
! intermediate structures are created)
RefinePathTo 100

! Set the minimization to use conjugate gradient
ConjugateGradient

! Set the exchange calculator to the typical one
! (default)
Set ExchangeCalculator 1

! Generate an initial path and save the initial
! path
MakeInitialPath
WriteTecPlotPath initialpath.tec

! Run the NEB using the initial and write the
! output file
PathMinimize
WriteTecPlotPath finalpath.tec
\end{verbatim}


%
% Discussion
%
\section{Discussion}
MERRILL presents an easy to use tool for the Rock Magnetic community to
study magnetic materials using techniques familiar and useful to them.
It is particularly focused on finding remanent states and studying their
stability. The parallels and usefulness to Paleomagnetic and Rock Magnetic
studies should be clear.
MERRILL has been used in a number of publications and talks using functionality
not presented in this paper,
e.g. behaviours of assemblages of interacting grains, large models, strongly
anisotropic materials and detailed simulations of magnetostrictive effects
\citep{Li2013,Chang2012,Williams2010,Conbhui2016}.

The simple scripting language makes it particularly friendly to non-technical
users. The fast and efficient minimization scheme means simple computer
experiments can be run quite quickly. The scripts presented here represent
the sort of scripts run by the authors day to day. Some effort has been
made to make these copy/pasteable, but a curious reader is recommended
to look in the ``demo'' directory of the MERRILL package for more, and more
complete examples.

If the scripting language is not up to a particular task, MERRILL can also
be used as a library and called from a Fortran program. In addition, a
plugin interface has been included so that users can compile libraries
that can be loaded from the BEMScript interface and hook into the
BEMScript parser to add commands and variables,
and also add effective field calculators to $\vec{H}^\texttt{eff}$
not originally shipped with MERRILL.
This should allow MERRILL to be adopted to a wide range of problems.

For viewing solutions, the authors recommend ParaView. It is a free, open
source 3D viewer, easily downloaded and installed from the ParaView website,
which can open and visualize solutions generated by MERRILL from the
{\bf WriteMagnetization} command. A plugin for ParaView for opening MERRILL
solutions with some pre-processing already done can be found in the demo
directory.

The correctness of MERRILL has been verified by running $\mu$MAG Standard
Problem 3, which tests for transitions of flower states to vortex states around
a critical point in the magnetic phase with respect to size.
This can be achieved by a simple size hysteresis script, varying a
cube edge length about $L = 8 l_\texttt{exch}$.
Our tests found a vortex state nucleated from a flower state at $8.0
l_\texttt{exch}$ for increasing size, and a flower state nucleated from a
vortex state around $6.6 l_\texttt{exch}$ to $6.5 l_\texttt{exch}$.
Another test of correctness involves solving the demagnetizing field for
a uniform sphere.
In this case, the demagnetizing field comes to
$\vec{H}^\texttt{dmag} = \frac{1}{3} \vec{M}$
independent of the sphere size, as expected.


\bibliographystyle{agufull08}
\bibliography{bibliography}

\end{article}
\end{document}
